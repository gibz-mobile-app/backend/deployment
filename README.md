## GKE Cluster erstellen

Create an autopilot cluster named `next-gibz-app` within zone `europe-west6` (Zurich) for the project named `gibz-app`.

```shell
gcloud container clusters create-auto next-gibz-app \
    --region europe-west6 \
    --project=gibz-app
```

For further interaction with the cluster, `kubectl` needs to be configured with the new clusters credentials.

```bash
gcloud container clusters get-credentials next-gibz-app \
    --region europe-west6 \
    --project=gibz-app
```

## Ingress

The ingress is responsible to recieve http requests and route them to the specific services within the k8s cluster.

There's a nice description of various concerns regarding the creation of a GKE ingress and related tasks on Medium: [https://engineering.sada.com/google-kubernetes-engine-ingress-configuration-for-production-environments-9677a303166a](https://engineering.sada.com/google-kubernetes-engine-ingress-configuration-for-production-environments-9677a303166a)

### IP Address

[https://cloud.google.com/kubernetes-engine/docs/how-to/managed-certs](https://cloud.google.com/kubernetes-engine/docs/how-to/managed-certs)

Register a *reserved (static) external IP address* named `next-gibz-app`:

```sh
gcloud compute addresses create next-gibz-app --global
```

Use the following command to check the IP address:

```sh
gcloud compute addresses describe next-gibz-app --global
```

### Google-managed TLS certificate

Create the resource as seen in [Ingress/managed_certificate.yml](Ingress/managed_certificate.yml).

Apply the manifest to the cluster:

```sh
kubectl apply -f Ingress/managed_certificate.yml
```

### Create the ingress

Create the resource as seen in [Ingress/ingress.yml](Ingress/ingress.yml).

Apple the manifest to the cluster:

```sh
kubectl apply -f Ingress/ingress.yml
```

### Add a BackendConfig

The BackendConfig object is used to configure custom health check options for any service which references the BackendConfig.

There's a template available at [Ingress/backend_config.yml](Ingress/backend_config.yml). Use the following command to add the backend config to the cluster:

```sh
kubectl apply -f Ingress/backend_config.yml
```

### Configure the DNS records

Use the command `kubectl get ingress` to view the ingresses IP address (`ADDRESS`). According to Googles documentation, it might take up to 20 minutes for the ingress to get an ip address assigned. It should although correspond to the earlier reserved (static) ip address.

Configure the corresponding DNS records to point to the IP address of the load balancer.



## Database

For financial reasons, although all data-related services use their own database, all databases are to be hosted on the same database server. For this, create a managed database instance in Google Cloud.

### Create databases

Use the following command to create a separate database for any backend services which require databases.

```sh
gcloud sql databases create <DATABASENAME> --instance <INSTANCE-NAME>
```

### Create database users

Since all databases should be as separated as possible, it's recommended to create a separate database user for every backend service.

Use the following command to create a database user:

```sh
gcloud sql users create <USERNAME> \
    --host=% \
    --instance=<INSTANCE-NAME> \
    --password=<PASSWORD>
```

### Adding users credentials as k8s secrets

Use the following command to add the specific database connection information as secrets to the k8s cluster:

```sh
kubectl create secret generic -n <NAMESPACE> <SECRET-NAME> \
    --from-literal=database=<DB-NAME> \
    --from-literal=username=<USERNAME> \
    --from-literal=password=<PASSWORD>
```

### Creating and linking *Google Service Account* and *Kubernetes Service Account*

In short, the database connection of a backend service is established through an additional container within the pod (`sidecar`) running the *Cloud SQL Auth proxy*. For this to work, a *Google Service Account (GSA)* must be created and linked to a dedicated *Kubernetes Service Account (KSA)*. The actual linking between GSA and KSA is done by enabling *Workload Identity* on the k8s cluster.

#### Create the *Google Service Account (GSA)*

Use the following command to create the *Google Service Account*:

```sh
gcloud iam service-accounts create <GSA-NAME>
```

The newly created GSA must be linked to a role which allows the GSA to establish a connection to the Cloud SQL instance:

```sh
gcloud projects add-iam-policy-binding gibz-app \
    --member "serviceAccount:<GSA-NAME>@gibz-app.iam.gserviceaccount.com" \
    --role "roles/cloudsql.client"
```

#### Create the *Kubernetes Service Account (KSA)* and link it to the GSA

Create a KSA using the following command:

```sh
kubectl create serviceaccount <KSA-NAME> -n <NAMESPACE>
```

Enable the IAM binding between GSA and KSA using the following command:

```sh
gcloud iam service-accounts add-iam-policy-binding \
    --role="roles/iam.workloadIdentityUser" \
    --member="serviceAccount:gibz-app.svc.id.goog[<NAMESPACE>/<KSA-NAME>]" \
    <GSA-NAME>@gibz-app.iam.gserviceaccount.com
```

Finally, add an annotation to complete the binding using this command:

```sh
kubectl annotate serviceaccount -n <NAMESPACE> \
    <KSA-NAME> \
    iam.gke.io/gcp-service-account=<GSA-NAME>@gibz-app.iam.gserviceaccount.com
```

## Adding Secrets

Configuration values provided as environment variables may be provided as secrets. Use the following command to add a secret to the k8s cluster:

```sh
kubectl create secret generic <NAME> \
    --from-literal=<KEY1>=<VALUE1> \
    --from-literal=<KEY2>=<VALUE2>
```

The line containing `--from-literal=<KEY>=<VALUE>` may be repeated multiple times to add additional entries to the secret.

More information: [https://kubernetes.io/docs/tasks/configmap-secret/managing-secret-using-kubectl/](https://kubernetes.io/docs/tasks/configmap-secret/managing-secret-using-kubectl/)
